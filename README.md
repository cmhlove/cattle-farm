# AI+养牛系统

## 介绍
将养殖业各环节打通，实现调控统一集中管理，规模化养殖成为现实，推动整个养殖行业的升级



## 手机端部分截图

![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/introduce/home.jpg)



![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/introduce/search_bull.jpg)

![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/introduce/search_cow.jpg)

![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/introduce/search_fatten.jpg)

![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/introduce/produce.jpg)

![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/introduce/user.jpg)



## 管理端部分截图

![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/gl_intronduce/bull.png)



![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/gl_intronduce/cow.png)



![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/gl_intronduce/post.png)



![alt 属性文本](https://zhyz-2024-app.oss-cn-beijing.aliyuncs.com/gl_intronduce/supplies.png)
